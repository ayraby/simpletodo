import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtodoinputComponent } from './addtodoinput.component';

describe('AddtodoinputComponent', () => {
  let component: AddtodoinputComponent;
  let fixture: ComponentFixture<AddtodoinputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtodoinputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtodoinputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

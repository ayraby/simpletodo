import { Component, Input, OnInit } from '@angular/core';
import { Todo } from '../todo';
import { TodosService } from '../todos.service'

@Component({
  selector: 'app-addtodoinput',
  templateUrl: './addtodoinput.component.html',
  styleUrls: ['./addtodoinput.component.css']
})
export class AddtodoinputComponent implements OnInit {
  todo: Todo;
  @Input('todoTitle') title: string;

  constructor(private service: TodosService) { }

  ngOnInit() {
    console.log("Initialisation of AddtodoInput");
    this.todo = new Todo();
  }

  submitTodo(event: any) {
    event.preventDefault();
    this.todo.title = event.target.title.value;
    this.service.add(this.todo);
    console.log(this.service.getTodos());
    this.todo = new Todo();
  }
}

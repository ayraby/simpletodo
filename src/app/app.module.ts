import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatSidenavModule,
  MatGridListModule,
  MatListModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatSelectModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { AddtodoinputComponent } from './addtodoinput/addtodoinput.component';
import { TodoslistComponent } from './todoslist/todoslist.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    AddtodoinputComponent,
    TodoslistComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatGridListModule,
    MatListModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

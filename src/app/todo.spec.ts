import { Todo } from './todo';

describe('Todo', () => {
  it('should create an instance', () => {
    expect(new Todo()).toBeTruthy();
  });


  it('should accept values', () => {
    let todo = new Todo({
      title: 'test',
      description: 'test',
      completed: true,
      priority: 'high',
    });
    expect(todo.title).toEqual('test');
    expect(todo.description).toEqual('test');
    expect(todo.completed).toEqual(true);
    expect(todo.priority).toEqual('high');
  });

});

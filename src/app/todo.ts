let defaultID = 0;

export class Todo {
  public id: number = ++defaultID;
  public title: string = '';
  public description: string = '';
  public completed: boolean = false;
  public priority: string = 'A';

  constructor(values: Object = {}) {
    Object.assign(this, values);
    defaultID = this.id;
  }

  public priorities: Array<string> = ['A', 'B', 'C', 'D'];
}

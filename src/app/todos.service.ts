import { Injectable } from '@angular/core';
import { Todo } from './todo';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  todos: Todo[] = [];

  constructor() { }

  getTodos(): Todo[] {
    return this.todos;
  }

  add(todo: Todo): TodosService {
    this.todos.push(todo);
    return this;
  }

  delete(id: number): Todo[] {
    this.todos = this.todos.filter(todo => todo.id !== id);
    return this.todos;
  }

  update(id: number, attributes: Object = {}): TodosService {
    Object.assign(
      this.todos[this.todos.findIndex(todo => todo.id === id)],
      attributes
    );
    return this;
  }
}

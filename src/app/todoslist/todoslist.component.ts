import { Component, OnInit } from '@angular/core';
import { TodosService } from '../todos.service';
import { Todo } from '../todo';

@Component({
  selector: 'app-todoslist',
  templateUrl: './todoslist.component.html',
  styleUrls: ['./todoslist.component.css']
})
export class TodoslistComponent implements OnInit {
  todos: Todo[];

  constructor(private service: TodosService) { }

  ngOnInit() {
    this.todos = this.service.getTodos();
  }

  deleteTodo(event: any, id: string) {
    event.preventDefault();
    this.todos = this.service.delete(parseFloat(id));
  }

  updatePriority(event: any, id: string) {
    event.preventDefault();
    this.service.update(parseFloat(id), { priority: event.target.value })
  }

}
